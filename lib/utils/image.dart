import "dart:async";
import "dart:typed_data";
import "dart:ui";

import "package:flutter/material.dart";

Future<Uint8List> loadImage(String url) {
  ImageStreamListener listener;

  final Completer<Uint8List> completer = Completer<Uint8List>();
  final ImageStream imageStream =
      AssetImage(url).resolve(ImageConfiguration.empty);

  listener = ImageStreamListener(
    (ImageInfo imageInfo, bool synchronousCall) {
      imageInfo.image.toByteData(format: ImageByteFormat.png)
          .then((data) => completer.complete(data!.buffer.asUint8List()));
    },
    onError: (dynamic exception, StackTrace? stackTrace) {
      completer.completeError(exception);
    },
  );

  imageStream.addListener(listener);

  return completer.future.whenComplete(() => imageStream.removeListener(listener));
}

class ImageWithPlaceholder extends StatelessWidget {
  const ImageWithPlaceholder({
    Key? key,
    required this.image,
    required this.placeholder,
  }) : super(key: key);

  final ImageProvider image;
  final Widget placeholder;

  @override
  Widget build(BuildContext context) {
    return Image(
      image: image,
      frameBuilder: (context, child, frame, wasSynchronouslyLoaded) {
        if (wasSynchronouslyLoaded)
          return child;
        else
          return AnimatedSwitcher(
            duration: const Duration(milliseconds: 200),
            child: frame != null ? child : placeholder,
          );
      },
    );
  }
}
