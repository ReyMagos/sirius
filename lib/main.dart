import "package:flutter/material.dart";
import "package:sirius/views/home/home.dart";
import "package:sirius/views/onboarding.dart";

import "package:stack/stack.dart" as data;


void main() {
  runApp(App.instance);
}

final appKey = new GlobalKey<_SApp>();

class App extends StatefulWidget {
  static final App instance = App(key: Key("app"));
  final data.Stack<Widget> viewStack = data.Stack();

  static viewDestroyed() {
    App.instance.viewStack.pop();
  }

  static viewAdded(Widget view) {
    App.instance.viewStack.push(view);

  }

  App({required Key? key}) : super(key: key) {
    viewStack.push(HomeView());
    viewStack.push(OnboardingView());
  }

  _SApp createState() => _SApp();
}

class ViewChanged extends Notification {

}

class AppState extends State<App> {
  Widget? currentView;

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.ltr,
      child: currentView,
    );
  }
}
