import "package:flutter/material.dart";
import "package:flutter/widgets.dart";

import "tabs/tabs.dart";

/*
 * Main view of app
 */
class HomeView extends StatefulWidget {
  _SHome createState() => _SHome();
}

class _SHome extends State<HomeView> {
  Map<String, Widget> tabs = {
    "Заказы": OrdersTab(),
    "Клинеры": CleanersTab(),
    "Бонусы": BonusesTab(),
    "Помощь": HelpTab(),
  };

  String currentTab = "Заказы";

  void tabChanged(String tab) {
    setState(() {
      currentTab = tab;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Expanded(
            child: tabs[currentTab]!,
          ),
          HomeMenu(
            tabs: tabs.keys.toList(),
            selectedTab: currentTab,
            onSelect: tabChanged,
          )
        ],
      ),
      decoration: BoxDecoration(
        color: Colors.white,
      ),
    );
  }
}

/*
 * Menu bar at the home page
 */
class HomeMenu extends StatelessWidget {
  final Color defaultColor = Color(0xFFBEC4FB);
  final Color selectedColor = Color(0xFF3656F9);

  final List<String> tabs;
  final Function(String) onSelect;
  final String selectedTab;

  HomeMenu({required this.tabs, required this.selectedTab,
    required this.onSelect});

  List<Widget> generateTabs() {
    List<Widget> result = [];
    for (String tab in tabs) {
      var currentColor = tab == selectedTab ? selectedColor : defaultColor;

      result.add(ElevatedButton(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(Icons.code),
            Text(
              tab,
              style: TextStyle(
                fontFamily: "Eastman",
                fontSize: 12,
              ),
            ),
          ],
        ),
        onPressed: () => onSelect(tab),
        style: ButtonStyle(
          shape: MaterialStateProperty.all(RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20)
          )),
          foregroundColor: MaterialStateProperty.all(currentColor),
          backgroundColor: MaterialStateProperty.all(Color(0xFFF6F7FF)),
          shadowColor: MaterialStateProperty.all(Colors.transparent),
        ),
      ));
    }
    return result;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: generateTabs(),
      ),
      height: 100,
      margin: const EdgeInsets.all(20),
      decoration: BoxDecoration(
        color: Color(0xFFF6F7FF),
        borderRadius: BorderRadius.circular(20),
      ),
    );
  }
}
