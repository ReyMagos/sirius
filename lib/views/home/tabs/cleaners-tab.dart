import "package:flutter/material.dart";


class CleanersTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(Icons.favorite, color: Colors.grey, size: 40),
        SizedBox(height: 20),
        Container(
          child: Text(
            "Здесь будут клинеры, которые вам понравятся",
            style: TextStyle(
              color: Colors.grey,
              fontFamily: "Eastman",
            ),
            textAlign: TextAlign.center,
          ),
          width: 200,
        )
      ],
    );
  }
}
