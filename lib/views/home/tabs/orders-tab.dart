import "package:flutter/material.dart";


class OrdersTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        SizedBox(height: 50),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text(
              "Актуальные",
              style: TextStyle(
                color: Colors.black,
                fontFamily: "Eastman",
                fontSize: 18,
              ),
            ),
            Text(
              "Выполненные",
              style: TextStyle(
                color: Colors.black,
                fontFamily: "Eastman",
                fontSize: 18,
              ),
            ),
          ],
        ),
        Padding(
          padding: EdgeInsets.all(20),
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Padding(
                  padding: EdgeInsets.only(left: 20, right: 20, top: 20),
                  child: Image(
                    image: AssetImage("assets/images/home/order.png"),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(20),
                  child: Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(top: 20),
                          child: Text(
                            "Закажите уборку",
                            style: TextStyle(
                              color: Colors.black,
                              fontFamily: "Eastman",
                              fontSize: 25,
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(bottom: 30, top: 5),
                          child: Container(
                            child: Text(
                              "Пришлём проверенного клинера "
                              "и наведем порядок в вашем доме",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Color(0xFF9397AE),
                                fontFamily: "Eastman",
                                height: 1.5,
                              ),
                            ),
                            width: 220,
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(Icons.flash_on, color: Color(0xFFFEB471)),
                            Text(
                              " от 650 ₽",
                              style: TextStyle(
                                color: Colors.black,
                                fontFamily: "Eastman",
                                fontSize: 18,
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: EdgeInsets.only(bottom: 20, top: 15),
                          child: ElevatedButton(
                            onPressed: () =>,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  "Заказать уборку ",
                                  style: TextStyle(
                                    fontFamily: "Eastman",
                                  ),
                                ),
                                Icon(
                                  Icons.keyboard_arrow_right,
                                  size: 20,
                                ),
                              ],
                            ),
                            style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all(
                                  Color(0xFF1E88E5),
                              ),
                              foregroundColor: MaterialStateProperty.all(
                                  Colors.white,
                              ),
                              shape: MaterialStateProperty.all(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15),
                                ),
                              ),
                              fixedSize: MaterialStateProperty.all(
                                  Size(268, 70),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20),
                    ),
                  ),
                ),
              ],
            ),
            decoration: BoxDecoration(
              color: Color(0xFFEDF0FF),
              borderRadius: BorderRadius.circular(20),
            ),
          ),
        ),
      ],
    );
  }
}


class RoomDetails extends StatefulWidget {
  _SRoomDetails createState() => _SRoomDetails();
}

class _SRoomDetails extends State<RoomDetails> {
  final ButtonStyle defaultStyle = ButtonStyle(

  );

  final ButtonStyle selectedStyle = ButtonStyle(

  );

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            ElevatedButton(
              onPressed: null,
              child: Icon(Icons.arrow_back_ios),
              style: defaultStyle,
            ),
            Text("Ввести промокод")
          ],
        ),
        Image(image: AssetImage("assets/images/home/tabs/room.png")),
        Container(

        ),
      ],
    );
  }
}


class OrderDetails extends StatefulWidget {
  _SOrderDetails createState() => _SOrderDetails();
}

class _SOrderDetails extends State<OrderDetails> {
  @override
  Widget build(BuildContext context) {
    throw UnimplementedError();
  }
}
