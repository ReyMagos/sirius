import "dart:convert";

import "package:flutter/material.dart";
import "package:flutter/services.dart";
import "package:flutter/widgets.dart";

import "../main.dart";

/*
 * Widget that introduces app to user
 * Appears once at first opening
 */
class OnboardingView extends StatefulWidget {
  OnboardingView();

  _OnboardingState createState() => _OnboardingState();
}

class _OnboardingState extends State<OnboardingView> {
  final contentPath = "assets/content";
  final imagesPath = "assets/images";

  int page = 1;
  List<Future<dynamic>> assets = [];

  Future<String> loadAsset(String path) async {
    return await rootBundle.loadString(path);
  }

  @override
  Widget build(BuildContext context) {
    assets = [
      loadAsset("$contentPath/onboarding/$page.json"),
      precacheImage(AssetImage("$imagesPath/onboarding/$page.png"), context)
    ];

    return FutureBuilder(
      future: Future.wait(assets),
      builder: (BuildContext context, AsyncSnapshot<List<dynamic>> snapshot) {
        Widget view;

        if (snapshot.connectionState == ConnectionState.done) {
          final jsonData = jsonDecode(snapshot.requireData[0]);
          String title = jsonData["title"];
          String description = jsonData["description"];
          List<String> hexs = jsonData["colors"].cast<String>();
          List<Color> colors = [];
          for (String hex in hexs) colors.add(Color(int.parse(hex)));

          AssetImage image = AssetImage("$imagesPath/onboarding/$page.png");

          view = Container(
            child: Column(
              children: [
                Expanded(
                  flex: 2,
                  child: Image(image: image),
                ),
                Text(
                  title,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 40,
                      height: 1.25,
                      fontFamily: "Eastman",
                      color: colors[1]),
                ),
                SizedBox(height: 10),
                Container(
                    width: 350,
                    child: Text(
                      description,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 16,
                          height: 1.5,
                          fontFamily: "Eastman",
                          color: colors[2]),
                    )
                ),
                Expanded(
                  child: Center(
                    child: ElevatedButton(
                      onPressed: () {
                        setState(() {
                          ++page;
                          if (page == 4) App.viewDestroyed();
                        });
                      },
                      child: Icon(Icons.arrow_forward_ios),
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(colors[3]),
                        foregroundColor: MaterialStateProperty.all(colors[0]),
                        fixedSize: MaterialStateProperty.all(Size(50, 60)),
                        shape: MaterialStateProperty.all(RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15))),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            decoration: BoxDecoration(color: colors[0]),
          );
        } else {
          view = Container(
            key: Key("placeholder"),
            decoration: BoxDecoration(color: Colors.white),
          );
        }

        return AnimatedSwitcher(
          duration: const Duration(milliseconds: 500),
          transitionBuilder: (Widget child, Animation<double> animation) {
            var currentAnimation;
            if (animation.status == AnimationStatus.reverse)
              currentAnimation = Tween<Offset>(begin: Offset.zero, end: Offset.zero);
            else
              currentAnimation = Tween<Offset>(
                      begin: const Offset(1.0, 0.0),
                      end: const Offset(0.0, 0.0)
              ).chain(CurveTween(curve: Curves.linearToEaseOut));

            return ClipRect(
                child: SlideTransition(
                  position: currentAnimation.animate(animation),
                  child: child,
              )
            );
          },
          layoutBuilder: (Widget? currentChild, List<Widget> previousChildren) {
            List<Widget> children = previousChildren;
            if (currentChild != null)
              children = children.toList()..add(currentChild);
            return Stack(
              children: children,
              alignment: Alignment.center,
            );
          },
          child: view,
        );
      },
    );
  }
}
